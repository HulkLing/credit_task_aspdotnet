﻿$(document).ready(function() {
    $("#count").keyup(
        function(event) {
            if (event.keyCode === 13) {
                var table = document.querySelector("#table");
                table.innerHTML = '';
                for (var i = 1; i <= this.value; i++) {

                    var li = document.createElement("li");
                    var input = document.createElement("input");
                    li.appendChild(input);
                    li.setAttribute("class", "item");
                    input.setAttribute("class", "items");
                    $(input).bind("change keyup input click",
                        function() {
                            NumberValidate(this);
                        });
                    table.appendChild(li);
                }

            }
        });
    $("#count").bind("change keyup input click",
        function() {
            NumberValidate(this);
        });


    $("#result").click(function(e) {
        var str = "";
        $.each($(".items"),
            function(i, val) {
                str += val.value + ",";
            });

        str = str.substring(0, str.length - 1);
        $.ajax({
            url: '/Home/GetData',
            dataType: 'json',
            data: { dataS: str },
            success: function(data) {
                if (data.length !== 0) {
                    $(".result").empty();
                    $(".result").append("<tr><td>" +
                        "От" +
                        "</td><td>" +
                        "К" +
                        "</td></tr>");
                    $.each(data,
                        function(i, value) {
                            $(".result").append("<tr><td>" +
                                value.Item1 +
                                "</td><td>" +
                                value.Item2 +
                                "</td></tr>");
                        });
                    $("#blok1").show();
                    $("#message").text("Количество: " + data.length);
                } else {
                    $("#message").text("-1");
                    $("#blok1").hide();
                }
            },
            error: function(data) {
                alert('Error');
            }
        });
        e.preventDefault();
    });

});

function NumberValidate(e) {
    if (e.value.match(/[^0-9]/g)) {
        e.value = e.value.replace(/[^0-9]/g, '');
    }
    checkParams();
};

function checkParams() {
    var count = $("#count").val();
    var key = true;
    var items = $(".items");

    $.each(items,
        function(i, val) {
            if (val.value.length === 0) {
                key = false;
            }
        });

    if (items.length > 0 && key && count.length !== 0) {
        $("#result").removeAttr("disabled");
    } else {
        $("#result").attr("disabled", "disabled");
    }

}