﻿using System;
using System.Collections.Generic;
using System.Linq;
using credit_task_aspdotnet.Models;

namespace credit_task_aspdotnet.Logics
{
    public class Methods
    {
        public static List<Tuple<int, int>> Result(Student[] pStudent)
        {
            Queue<Student> ready = new Queue<Student>();
            ready.Enqueue(pStudent[0]);
            Queue<Student> students = new Queue<Student>();
            IOrderedEnumerable<Student> y = pStudent.Skip(1).OrderByDescending(x => x.MessagesCount);
            foreach (Student p in y)
            {
                students.Enqueue(p);
            }
            List<Tuple<int, int>> result = new List<Tuple<int, int>>();

            while (students.Any() && ready.Any())
            {
                Student sender = ready.Dequeue();
                for (var i = 0; i < sender.MessagesCount; i++)
                {
                    if (!students.Any())
                        break;
                    Student recipient = students.Dequeue();
                    Tuple<int, int> t = Tuple.Create(sender.Index, recipient.Index);
                    result.Add(t);
                    ready.Enqueue(recipient);
                }
            }
            if (students.Any())
            {
                return new List<Tuple<int, int>>();
            }

            return result;
        }
    }
}