﻿using System;
using System.Linq;
using System.Web.Mvc;
using credit_task_aspdotnet.Logics;
using credit_task_aspdotnet.Models;

namespace credit_task_aspdotnet.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ViewResult Index()
        {
            var hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Доброе утро" : "Доброго дня";

            return View();
        }

        public ActionResult GetData(string dataS)
        {
            var a = dataS?.Split(',').Select(int.Parse).ToArray();
            if (a != null)
            {
                var n = a.Length;
                var b = new Student[n];
                for (var i = 0; i < n; i++)
                    b[i] = new Student {Index = i + 1, MessagesCount = a[i]};
                return Json(Methods.Result(b), JsonRequestBehavior.AllowGet);
            }

            return Json("Ошибка", JsonRequestBehavior.AllowGet);
        }
    }
}